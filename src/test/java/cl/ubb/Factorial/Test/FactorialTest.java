package cl.ubb.Factorial.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import cl.ubb.Factorial.Factorial;

public class FactorialTest {

	@Test
	public void factorialDeCeroEsCero() {
		int resultado;
		
		//arrange
		Factorial factorial = new Factorial();
		
		//act
		resultado = factorial.calcularFactorial(0);
		
		//assert
		assertEquals(0,resultado);
		
	}
	
	@Test
	public void factorialDeUnoEsUno(){
		int resultado;
		
		//arrange
		Factorial factorial = new Factorial();
		
		//act
		resultado = factorial.calcularFactorial(1);
		
		//assert
		assertEquals(1,resultado);
		
	}
	
	@Test
	public void factorialDeDosEsDos(){
		int resultado;
		
		//arrange
		Factorial factorial = new Factorial();
		//act
		resultado = factorial.calcularFactorial(2);
		//assert
		assertEquals(2,resultado);
		
	}
	
	@Test
	public void factorialDeTresEsSeis(){
		int resultado;
		
		//arrange
		Factorial factorial = new Factorial();
		//act
		resultado = factorial.calcularFactorial(3);
		//assert
		assertEquals(6,resultado);
		
	}
	
	@Test
	public void factorialDeCuatroEsVentiCuatro(){
		int resultado;
		
		//arrange
		Factorial factorial = new Factorial();
		//act
		resultado = factorial.calcularFactorial(4);
		//assert
		assertEquals(24,resultado);
		
	}
}
